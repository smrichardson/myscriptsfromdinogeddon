﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CrystalCollection : MonoBehaviour {

    public List<int> crystalCount;

	// Use this for initialization
	void Start () 
    {
        crystalCount = new List<int>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "TimeCrystal")
        {
            Destroy(other.gameObject);
            crystalCount.Add(1);
        }
    }
}
