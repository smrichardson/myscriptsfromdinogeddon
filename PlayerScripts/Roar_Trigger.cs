﻿using UnityEngine;
using System.Collections;

public class Roar_Trigger : MonoBehaviour {

    void OnTriggerEnter(Collider enemy)
    {
        if (enemy.tag == "EnemyToScare")
        {            
            //Scare Enemy
            enemy.gameObject.GetComponent<MeleeAI>().Scared();
        }
    }
}
