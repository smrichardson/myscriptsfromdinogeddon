﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TimeChange : MonoBehaviour
{
	public bool playAnim;
	Animation anim;
	public float charge = 1;
	public double maxCharge = 1.125;
	public float chargeSpeed= 2;
	GameObject timeTrigger;
	public List<Collider> timeObjects;
	Transform player;
	bool animPlayed = false;
	Vector3 originalSize;
	public bool lerp = false;

	// Use this for initialization
	void Start()
	{
		timeTrigger = GameObject.FindGameObjectWithTag("TimeTrigger");
		player = GameObject.FindGameObjectWithTag("Player").transform;
		originalSize = timeTrigger.transform.localScale;
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		if (Input.GetButton("Roar"))
		{
			transform.parent = player;
			if (charge <= maxCharge)
			{
				charge += Time.deltaTime * chargeSpeed;
				timeTrigger.transform.localScale *= charge;
                //GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                //GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
                //GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;
			}
		}
		if (/*playAnim &&*/ Input.GetButtonUp("Roar"))
		{
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
			lerp = true;
			if (!animPlayed)
			{
				foreach (Collider timeObj in timeObjects)
				{
					anim = timeObj.GetComponent<Animation>();
					anim.Play();
					animPlayed = true;
					if (animPlayed)
					{
						Vector3.Lerp(timeTrigger.transform.localScale, originalSize, Time.deltaTime * chargeSpeed);
					}
				}
				timeObjects.Clear();
				charge *= Time.deltaTime * -chargeSpeed;
				timeTrigger.transform.localScale *= charge;
			}
		}
		if (lerp)
		{
			transform.localScale = Vector3.Lerp(originalSize, timeTrigger.transform.localScale, charge);
			lerp = false;
			charge = 1;
		}
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "TimeObject")
		{
			timeObjects.Add(other);
			playAnim = true;
		}
	}
}
