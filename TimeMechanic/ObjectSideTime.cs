﻿using UnityEngine;
using System.Collections;

public class ObjectSideTime : MonoBehaviour {

	public int crystalsRequired;
	public Animation anim;

    bool animPlayed;
    public bool repeatable;

    GameObject player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        anim = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "TimeTrigger")
		{
            if(player.GetComponent<Player_Handler>().ReturnNumberofCyrstals() >= crystalsRequired)
            {
                if (anim != null && !animPlayed)
                {
                    anim.Play();
                    animPlayed = true;
                }
                if(repeatable)
                {
                    animPlayed = false;
                }                
            }
		}
	}
}
