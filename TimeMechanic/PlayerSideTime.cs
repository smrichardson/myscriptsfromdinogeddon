﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GameObject))]

public class PlayerSideTime : MonoBehaviour {

	public GameObject Trigger;
	GameObject TriggerClone;
	bool isObject = false;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		TriggerClone = GameObject.FindGameObjectWithTag("TimeTrigger");
		if (Input.GetButton("Roar") && !isObject)
		{
			TriggerClone = Trigger;
			isObject = true;
			Instantiate(TriggerClone, transform.position, Quaternion.identity);
			//GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
		}
		if(Input.GetButtonUp("Roar"))
		{
			isObject = false;
			//GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
		}
		if (TriggerClone != null && !isObject)
		{
			Destroy(TriggerClone);
		}
	}
}
