﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour {

	Button startButton;
	

	// Disables the selectable UI element directly below the Start Button
	public void IgnoreSelectables()
	{
		//Finds the selectable UI element below the start button and assigns it to a variable of type "Selectable"
		Selectable secondButton = startButton.FindSelectableOnDown();
		//Disables interaction with the selectable UI element
		secondButton.interactable = false;
	}

}
