﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TitleSoundScript : MonoBehaviour, ISelectHandler, ISubmitHandler
{
    public AudioSource source;
    public AudioClip clickSound;
    public AudioClip selectSound;

    Selectable sel;
    
    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlaySound()
    {
        source.PlayOneShot(clickSound);
    }

    public void OnSelect(BaseEventData eventData)
    {
        source.PlayOneShot(selectSound);
    }

    public void OnSubmit(BaseEventData eventData)
    {
        PlaySound();
    }
}