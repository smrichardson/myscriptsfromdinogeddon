﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class PauseMenu : MonoBehaviour
{
    public GameObject quittingPanel;
	public GameObject pauseMenu;
	//GameObject player;
	Canvas canvas;
	GameObject TempMenu;

	Vector3 panelPos = new Vector3();

	GameObject videoPanel;
	bool videoPanelOpen = false;

	GameObject audioPanel;
	bool audioPanelOpen = false;

	GameObject controlsPanel;
	bool controlsPanelOpen = false;

	bool pauseMenuOpen;

    Vector3 resumeGamePos;
    Vector3 optionsPos;
    Vector3 resetPos;
    Vector3 exitGamePos;

	public GameObject optionsMenu;
	GameObject[] all;
	List<GameObject> optionsButtons;
	GameObject o;

	MenuButtons buttons = new MenuButtons();
	bool paused = false;

    GameObject camera;
    [SerializeField]
    GameObject player;

	// Use this for initialization
	void Start () 
	{
		pauseMenuOpen = false;
        canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
		//player = GameObject.FindGameObjectWithTag("Player");
        camera = GameObject.FindGameObjectWithTag("MainCamera");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButtonDown("Start") && !pauseMenuOpen)
		{
			if (TempMenu != null)
			{
                Destroy(TempMenu);
			}
			paused = true;
			PauseGame();
		}
		else if (Input.GetButtonDown("Start") && !pauseMenuOpen)
		{
			ResumeGame();
		}
		if (Input.GetButtonDown("Cancel") && !videoPanelOpen && !audioPanelOpen && !controlsPanelOpen)
		{
			CloseOptions();
		}
		else if (Input.GetButtonDown("Cancel") && videoPanelOpen)
		{
			VideoBackToOptions(videoPanel);
		}
		else if (Input.GetButtonDown("Cancel") && audioPanelOpen)
		{
			AudioBackToOptions();
		}
		else if (Input.GetButtonDown("Cancel") && controlsPanelOpen)
		{
			ControlsBackToOptions();
		}
	}

	void PauseGame()
	{
		player.GetComponent<Player_Controller>().enabled = false;
		player.GetComponent<Player_Handler>().enabled = false;
		if (!pauseMenuOpen)
		{
			pauseMenuOpen = true;
			TempMenu = Instantiate(pauseMenu);
		}
		TempMenu.transform.SetParent(canvas.transform, false);
        GameObject.Find("CoinText").GetComponent<Text>().text = "Coins = " + player.GetComponent<Player_Handler>().ReturnNumberofCoins().ToString();
        GameObject.Find("CrystalsText (1)").GetComponent<Text>().text = "Crystals = " + player.GetComponent<Player_Handler>().ReturnNumberofCyrstals().ToString();
        GameObject.Find("EggsText (2)").GetComponent<Text>().text = "Eggs = " + player.GetComponent<Player_Handler>().ReturnNumberofEggs().ToString();
		TempMenu.transform.FindChild("Panel (2)").transform.GetChild(0).GetComponent<Button>().Select();
        TempMenu.transform.FindChild("Panel (2)").transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { ResumeGame(); });
        TempMenu.transform.FindChild("Panel (3)").transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { CreateOptionsMenu(); });
        TempMenu.transform.FindChild("Panel (4)").transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { ResetLevel(); });
        TempMenu.transform.FindChild("Panel (5)").transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { OpenQuittingPanel(); }); 
		Time.timeScale = 0;
	}

	public void ResumeGame()
	{
		if (TempMenu != null || paused)
		{
			Destroy(TempMenu);
			player.GetComponent<Player_Controller>().enabled = true;
			player.GetComponent<Player_Handler>().enabled = true;
			paused = false;
			Time.timeScale = 1;
			pauseMenuOpen = false;
		}
	}

    public void ResetLevel()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }

	public void CreateOptionsMenu()
	{
		if (o != null)
		{
			Destroy(o);
		}
		o = Instantiate(optionsMenu);
		o.transform.SetParent(canvas.transform, false);
		o.transform.localPosition = optionsMenu.transform.localPosition;
		all = GameObject.FindGameObjectsWithTag("PauseMenuButton");

        all[0].SetActive(false);

        all[1].SetActive(false);

        all[2].SetActive(false);

        all[3].SetActive(false);

        all[4].SetActive(false);

        optionsButtons = new List<GameObject>();
        optionsButtons.Add(canvas.transform.Find("OptionsMenu_Ingame(Clone)").Find("VideoButtonPanel").GetChild(0).gameObject);
        optionsButtons.Add(canvas.transform.Find("OptionsMenu_Ingame(Clone)").Find("AudioButtonPanel").GetChild(0).gameObject);
        optionsButtons.Add(canvas.transform.Find("OptionsMenu_Ingame(Clone)").Find("ControlsButtonPanel").GetChild(0).gameObject);
        optionsButtons.Add(canvas.transform.Find("OptionsMenu_Ingame(Clone)").Find("BackButtonPanel").GetChild(0).gameObject);

        optionsButtons[0].GetComponent<Button>().Select();
        optionsButtons[0].GetComponent<Button>().onClick.AddListener(delegate { VideoPanel(); });
        optionsButtons[1].GetComponent<Button>().onClick.AddListener(delegate { AudioPanel(); });
        optionsButtons[2].GetComponent<Button>().onClick.AddListener(delegate { ControlsPanel(); });
        optionsButtons[3].GetComponent<Button>().onClick.AddListener(delegate { CloseOptions(); });
	}

    public void CloseOptions()
    {
        if (o != null)
            Destroy(o);

        if (TempMenu != null)
        {
            TempMenu.transform.FindChild("Panel (1)").gameObject.SetActive(true);
            TempMenu.transform.FindChild("Panel (2)").gameObject.SetActive(true);
            TempMenu.transform.FindChild("Panel (3)").gameObject.SetActive(true);
            TempMenu.transform.FindChild("Panel (4)").gameObject.SetActive(true);
            TempMenu.transform.FindChild("Panel (5)").gameObject.SetActive(true);

            TempMenu.transform.FindChild("Panel (2)").transform.GetChild(0).GetComponent<Button>().Select();
        }
    }

	#region video
	public void VideoPanel()
	{
		videoPanelOpen = true;
		foreach (GameObject g in optionsButtons)
		{
			g.GetComponent<Button>().interactable = false;
		}

        GameObject optionsPanel = canvas.transform.FindChild("OptionsMenu_Ingame(Clone)").transform.GetChild(1).gameObject;
        videoPanel = canvas.transform.FindChild("OptionsMenu_Ingame(Clone)").transform.FindChild("VideoPanel").gameObject;

		panelPos = videoPanel.transform.localPosition;

		videoPanel.transform.localPosition = optionsPanel.transform.localPosition;

		videoPanel.transform.FindChild("Quality").transform.GetChild(0).GetComponent<Dropdown>().interactable = true;
		videoPanel.transform.FindChild("Quality").transform.GetChild(0).GetComponent<Dropdown>().Select();
		videoPanel.transform.FindChild("Quality").transform.GetChild(0).GetComponent<Dropdown>().value = QualitySettings.GetQualityLevel();
		videoPanel.transform.FindChild("Quality").transform.GetChild(0).GetComponent<Dropdown>().onValueChanged.AddListener(delegate { ChangeQuality(videoPanel.transform.FindChild("Quality").transform.GetChild(0).GetComponent<Dropdown>()); });

		videoPanel.transform.FindChild("VSync").GetComponent<Toggle>().interactable = true;
		if (QualitySettings.vSyncCount == 0)
		{
			videoPanel.transform.FindChild("VSync").GetComponent<Toggle>().isOn = false;
		}
		else
		{
			videoPanel.transform.FindChild("VSync").GetComponent<Toggle>().isOn = true;
		}
		videoPanel.transform.FindChild("VSync").GetComponent<Toggle>().onValueChanged.AddListener(delegate { VSync(videoPanel.transform.FindChild("VSync").GetComponent<Toggle>()); });

		videoPanel.transform.FindChild("Back").GetComponent<Button>().interactable = true;
		videoPanel.transform.FindChild("Back").GetComponent<Button>().onClick.AddListener(delegate { VideoBackToOptions(videoPanel); });
	}

	public void ChangeQuality(Dropdown o)
	{
		switch (o.value)
		{
			case 0:
				QualitySettings.SetQualityLevel(o.value);
				break;
			case 1:
				QualitySettings.SetQualityLevel(o.value);
				break;
			case 2:
				QualitySettings.SetQualityLevel(o.value);
				break;
			case 3:
				QualitySettings.SetQualityLevel(o.value);
				break;
			case 4:
				QualitySettings.SetQualityLevel(o.value);
				break;
			case 5:
				QualitySettings.SetQualityLevel(o.value);
				break;
		}
	}

	public void VSync(Toggle t)
	{
		if (t.isOn)
		{
			QualitySettings.vSyncCount = 2;
		}
		else
		{
			QualitySettings.vSyncCount = 0;
		}
	}

	public void VideoBackToOptions(GameObject o)
	{
		o.transform.localPosition = panelPos;
		o.transform.GetChild(1).GetChild(0).GetComponent<Dropdown>().interactable = false;
		o.transform.GetChild(2).GetComponent<Toggle>().interactable = false;
		o.transform.GetChild(3).GetComponent<Button>().interactable = false;

		foreach (GameObject g in optionsButtons)
		{
			g.GetComponent<Button>().interactable = true;
		}
		optionsButtons[0].GetComponent<Button>().Select();
		videoPanelOpen = false;
	}
	#endregion
	#region audio
	public void AudioPanel()
	{
		audioPanelOpen = true;
		foreach (GameObject g in optionsButtons)
		{
			g.GetComponent<Button>().interactable = false;
		}

        GameObject optionsPanel = canvas.transform.FindChild("OptionsMenu_Ingame(Clone)").transform.GetChild(1).gameObject;
        audioPanel = canvas.transform.FindChild("OptionsMenu_Ingame(Clone)").transform.FindChild("AudioPanel").gameObject;

		panelPos = audioPanel.transform.localPosition;

		audioPanel.transform.localPosition = optionsPanel.transform.localPosition;

		audioPanel.transform.FindChild("MasterVolume").GetChild(0).GetComponent<Slider>().interactable = true;
		audioPanel.transform.FindChild("MasterVolume").GetChild(0).GetComponent<Slider>().Select();
		audioPanel.transform.FindChild("MasterVolume").GetChild(0).GetComponent<Slider>().value = AudioListener.volume;
		audioPanel.transform.FindChild("MasterVolume").GetChild(0).GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeMasterVolume(audioPanel.transform.FindChild("MasterVolume").GetChild(0).GetComponent<Slider>()); });

		audioPanel.transform.FindChild("Mute").GetComponent<Toggle>().interactable = true;
		if (AudioListener.pause)
		{
			audioPanel.transform.FindChild("Mute").GetComponent<Toggle>().isOn = true;
		}
		else
		{
			audioPanel.transform.FindChild("Mute").GetComponent<Toggle>().isOn = false;
		}
		audioPanel.transform.FindChild("Mute").GetComponent<Toggle>().onValueChanged.AddListener(delegate { MuteAudio(audioPanel.transform.FindChild("Mute").GetComponent<Toggle>()); });

		audioPanel.transform.FindChild("Back").GetComponent<Button>().interactable = true;
		audioPanel.transform.FindChild("Back").GetComponent<Button>().onClick.AddListener(delegate { AudioBackToOptions(); });
	}

	public void ChangeMasterVolume(Slider s)
	{
		AudioListener.volume = s.value;
	}

	public void MuteAudio(Toggle t)
	{
		AudioListener.pause = t.isOn;
	}

	public void AudioBackToOptions()
	{
		audioPanel.transform.localPosition = panelPos;
		audioPanel.transform.GetChild(1).GetChild(0).GetComponent<Slider>().interactable = false;
		audioPanel.transform.GetChild(2).GetComponent<Toggle>().interactable = false;
		audioPanel.transform.GetChild(3).GetComponent<Button>().interactable = false;

		foreach (GameObject g in optionsButtons)
		{
			g.GetComponent<Button>().interactable = true;
		}
		optionsButtons[0].GetComponent<Button>().Select();
		audioPanelOpen = false;
	}

	#endregion
	#region controls

	public void ControlsPanel()
	{
		controlsPanelOpen = true;
		foreach (GameObject g in optionsButtons)
		{
			g.GetComponent<Button>().interactable = false;
		}

        GameObject optionsPanel = canvas.transform.FindChild("OptionsMenu_Ingame(Clone)").transform.GetChild(1).gameObject;
        controlsPanel = canvas.transform.FindChild("OptionsMenu_Ingame(Clone)").transform.FindChild("ControlsPanel").gameObject;

		panelPos = controlsPanel.transform.localPosition;

		controlsPanel.transform.localPosition = optionsPanel.transform.localPosition;

		controlsPanel.transform.FindChild("InvertY").GetComponent<Toggle>().interactable = true;
        	
        if (!camera.GetComponent<Camera_Control>().invert_cameraY)
        {
            controlsPanel.transform.FindChild("InvertY").GetComponent<Toggle>().isOn = false;
        }
        else
        {
            controlsPanel.transform.FindChild("InvertY").GetComponent<Toggle>().isOn = true;
        }
        controlsPanel.transform.FindChild("InvertY").GetComponent<Toggle>().Select();
        controlsPanel.transform.FindChild("InvertY").GetComponent<Toggle>().onValueChanged.AddListener(delegate { InvertY(); });

        controlsPanel.transform.FindChild("InvertX").GetComponent<Toggle>().interactable = true;

        if (!camera.GetComponent<Camera_Control>().invert_cameraX)
        {
            controlsPanel.transform.FindChild("InvertX").GetComponent<Toggle>().isOn = false;
        }
        else
        {
            controlsPanel.transform.FindChild("InvertX").GetComponent<Toggle>().isOn = true;
        }

        controlsPanel.transform.FindChild("InvertX").GetComponent<Toggle>().onValueChanged.AddListener(delegate { InvertX(); }); 
        
		controlsPanel.transform.FindChild("Back").GetComponent<Button>().interactable = true;
		controlsPanel.transform.FindChild("Back").GetComponent<Button>().onClick.AddListener(delegate { ControlsBackToOptions(); });
	}

	void ControlsBackToOptions()
	{
		controlsPanel.transform.localPosition = panelPos;
		controlsPanel.transform.GetChild(2).GetComponent<Toggle>().interactable = false;
		controlsPanel.transform.GetChild(3).GetComponent<Toggle>().interactable = false;
		controlsPanel.transform.GetChild(4).GetComponent<Button>().interactable = false;

		foreach (GameObject g in optionsButtons)
		{
			g.GetComponent<Button>().interactable = true;
		}
		optionsButtons[0].GetComponent<Button>().Select();
		controlsPanelOpen = false;
	}

    void InvertY()
    {
        if (!camera.GetComponent<Camera_Control>().invert_cameraY)
        {
            camera.GetComponent<Camera_Control>().InvertY(true);
        }
        else
        {
            camera.GetComponent<Camera_Control>().InvertY(false);
        }
    }

    void InvertX()
    {
        if (!camera.GetComponent<Camera_Control>().invert_cameraX)
        {
            camera.GetComponent<Camera_Control>().InvertX(true);
        }
        else
        {
            camera.GetComponent<Camera_Control>().InvertX(false);
        }
    }

	#endregion

	public void QuitGame()
	{
		Application.Quit();
	}

    public void OpenQuittingPanel()
    {
        if (o != null)
        {
            Destroy(o);
        }
        o = Instantiate(quittingPanel);
        o.transform.SetParent(canvas.transform, false);

        o.transform.FindChild("MainMenu").GetComponent<Button>().onClick.AddListener(delegate { BackToMenu(); });
        o.transform.FindChild("Desktop").GetComponent<Button>().onClick.AddListener(delegate { QuitGame(); });

        all = GameObject.FindGameObjectsWithTag("PauseMenuButton");

        all[3].SetActive(false);

        all[2].SetActive(false);

        all[4].SetActive(false);

        all[0].SetActive(false);

        all[1].SetActive(false);

        o.transform.FindChild("MainMenu").GetComponent<Button>().Select();
    }

    void BackToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
