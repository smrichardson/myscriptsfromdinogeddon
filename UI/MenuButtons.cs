﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MenuButtons : MonoBehaviour
{

    GameObject creditsPanel;
    public GameObject optionsMenu;
    public GameObject leaderboard_canvas;
    public List<GameObject> optionsButtons;
    public GameObject[] all;

    GameObject o;
    Canvas canvas;

    Vector3 panelPos = new Vector3();

    GameObject videoPanel;
    bool videoPanelOpen = false;

    GameObject audioPanel;
    bool audioPanelOpen = false;

    GameObject controlsPanel;
    bool controlsPanelOpen = false;

    Vector3 newGamePos;
    Vector3 optionsPos;
    Vector3 leaderBoardsPos;
    Vector3 creditsPos;
    Vector3 exitGamePos;

    public MovieTexture movie;
    public AudioSource audio;
    public AudioSource mainAudio;

    bool leaderboard_open;

    GameObject player;
    GameObject camera;

    void Start()
    {
        canvas = GameObject.Find("MenuCanvas").GetComponent<Canvas>();
        creditsPanel = GameObject.Find("CreditsVideo");
        creditsPanel.GetComponent<Image>().enabled = false;
        player = GameObject.FindGameObjectWithTag("Player");
        camera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            if (SceneManager.GetActiveScene().name == "MainMenu")
            {
                if (canvas.transform.Find("OptionsMenu(Clone)") != null)
                {
                    canvas.transform.Find("OptionsMenu(Clone)").Find("VideoButtonPanel").GetChild(0).GetComponent<Button>().Select();
                }
                else
                {
                    canvas.transform.Find("startPanel").GetChild(0).GetComponent<Button>().Select();
                }
            }
            else if(SceneManager.GetActiveScene().name == "ReleaseScene_2")
            {
                if (canvas.transform.Find("OptionsMenu_Ingame(Clone)") != null)
                {
                    canvas.transform.Find("OptionsMenu_Ingame(Clone)").Find("VideoButtonPanel").GetChild(0).GetComponent<Button>().Select();
                }
                else
                {
                    canvas.transform.Find("startPanel").GetChild(0).GetComponent<Button>().Select();
                }
            }
        }
        if (!movie.isPlaying)
        {
            creditsPanel.GetComponent<Image>().enabled = false;
            movie.Stop();
            mainAudio.mute = false;
        }
        if (Input.GetButtonDown("Cancel") && movie.isPlaying)
        {
            CreditsBack();
        }
        if (Input.GetButtonDown("Cancel") && !videoPanelOpen && !audioPanelOpen && !controlsPanelOpen && !movie.isPlaying)
        {
            CloseOptions();
        }
        else if(Input.GetButtonDown("Cancel") && videoPanelOpen)
        {
            VideoBackToOptions(videoPanel);
        }
        else if (Input.GetButtonDown("Cancel") && audioPanelOpen)
        {
            AudioBackToOptions();
        }
        else if (Input.GetButtonDown("Cancel") && controlsPanelOpen)
        {
            ControlsBackToOptions();
        }
    }

    public void NewGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void CreditsBack()
    {
        CloseOptions();
        movie.Stop();
        audio.Stop();
        mainAudio.mute = false;
        canvas.transform.GetChild(0).FindChild("CreditsBackPanel").gameObject.SetActive(false);
    }

    public void CreateOptionsPanel()
    {
        if (o != null)
        {
            Destroy(o);
        }
        o = Instantiate(optionsMenu);
        o.transform.SetParent(canvas.transform, false);
        o.transform.localPosition = optionsMenu.transform.localPosition;
        all = GameObject.FindGameObjectsWithTag("MainMenuButton");

        all[3].SetActive(false);

        all[2].SetActive(false);

        all[4].SetActive(false);

        all[0].SetActive(false);

        all[1].SetActive(false);

        SetUpOptions();
    }

    public void LeaderboardPanel()
    {
        leaderboard_open = true;
        if (o != null)
        {
            Destroy(o);
        }

        all = GameObject.FindGameObjectsWithTag("MainMenuButton");

        foreach (GameObject menuobject in all)
        {
            menuobject.SetActive(false);
        }

        o = Instantiate(leaderboard_canvas);
        o.transform.SetParent(canvas.transform, false);
        o.transform.position = canvas.transform.position;
        o.transform.GetChild(7).GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { CloseLeaderboard(); });
        o.transform.GetChild(7).GetChild(0).GetComponent<Button>().Select();
        Leaderboard_Handler leaderboard = new Leaderboard_Handler();
        GameObject HighScoresText = GameObject.Find("HighScores");
        Dictionary<string, int> highscores = leaderboard.GetHighScores();

        var sorted_scores = from pair in highscores
                            orderby pair.Value descending
                            select pair;

        Debug.Log(sorted_scores.Count());

        int score_count = 1;
        foreach (Transform child in HighScoresText.transform)
        {
            child.GetComponent<Text>().text = score_count.ToString() + ") " + sorted_scores.ElementAt(score_count - 1).Key + " = " + sorted_scores.ElementAt(score_count - 1).Value;
            score_count++;
        }

        GameObject FastTimesText = GameObject.Find("FastTimes");
        Dictionary<string, float> fasttimes = leaderboard.GetFastTimes();

        var sorted_times = from pair in fasttimes
                           orderby pair.Value ascending
                           select pair;

        int time_count = 1;
        foreach (Transform child in FastTimesText.transform)
        {
            float time = sorted_times.ElementAt(time_count - 1).Value;
            float minutes = (int)(Mathf.Floor(time / 60.0f));
            float seconds = (int)(time % 60.0f);
            float miliseconds = (int)((time * 10) % 10);

            string time_ = minutes + ":" + seconds + ":" + miliseconds;

            child.GetComponent<Text>().text = time_count.ToString() + ") " + sorted_times.ElementAt(time_count - 1).Key + " = " + time_;
            time_count++;
        }
    }

    public void CloseLeaderboard()
    {
        CloseOptions();
    }

    public void PlayCredits()
    {
        canvas.transform.GetChild(0).FindChild("CreditsBackPanel").gameObject.SetActive(true);
        canvas.transform.GetChild(0).FindChild("CreditsBackPanel").GetChild(0).GetComponent<Button>().onClick.AddListener(delegate {CreditsBack(); });
        canvas.transform.GetChild(0).FindChild("CreditsBackPanel").GetChild(0).GetComponent<Button>().Select();
        all = GameObject.FindGameObjectsWithTag("MainMenuButton");
        all[3].SetActive(false);
        all[2].SetActive(false);
        all[4].SetActive(false);
        all[0].SetActive(false);
        all[1].SetActive(false);

        creditsPanel.GetComponent<Image>().enabled = true;
        creditsPanel.GetComponent<Image>().material.mainTexture = movie as MovieTexture;
        audio.clip = movie.audioClip;
        movie.Play();
        audio.Play();
        mainAudio.mute = true;
    }

    public void SetUpOptions()
    {
        //optionsButtons = GameObject.FindGameObjectsWithTag("UIButton");

        optionsButtons = new List<GameObject>();
        optionsButtons.Add(canvas.transform.Find("OptionsMenu(Clone)").Find("VideoButtonPanel").GetChild(0).gameObject);
        optionsButtons.Add(canvas.transform.Find("OptionsMenu(Clone)").Find("AudioButtonPanel").GetChild(0).gameObject);
        optionsButtons.Add(canvas.transform.Find("OptionsMenu(Clone)").Find("ControlsButtonPanel").GetChild(0).gameObject);
        optionsButtons.Add(canvas.transform.Find("OptionsMenu(Clone)").Find("BackButtonPanel").GetChild(0).gameObject);

        optionsButtons[0].GetComponent<Button>().Select();
        optionsButtons[0].GetComponent<Button>().onClick.AddListener(delegate { VideoPanel(); });
        optionsButtons[1].GetComponent<Button>().onClick.AddListener(delegate { AudioPanel(); });
        optionsButtons[2].GetComponent<Button>().onClick.AddListener(delegate { ControlsPanel(); });
        optionsButtons[3].GetComponent<Button>().onClick.AddListener(delegate { CloseOptions(); });
    }

    public void CloseOptions()
    {
        if (o != null)
            Destroy(o);

        canvas.transform.FindChild("startPanel").gameObject.SetActive(true);
        canvas.transform.FindChild("OptionsButtonPanel").gameObject.SetActive(true);
        canvas.transform.FindChild("LeaderBoardButtonPanel").gameObject.SetActive(true);
        canvas.transform.FindChild("CreditsButtonPanel").gameObject.SetActive(true);
        canvas.transform.FindChild("QuitButtonPanel").gameObject.SetActive(true);

        canvas.transform.FindChild("startPanel").transform.GetChild(0).GetComponent<Button>().Select();
    }

    #region video
    public void VideoPanel()
    {
        videoPanelOpen = true;
        foreach (GameObject g in optionsButtons)
        {
            g.GetComponent<Button>().interactable = false;
        }

        GameObject optionsPanel = canvas.transform.FindChild("OptionsMenu(Clone)").transform.FindChild("Panel").gameObject;
        videoPanel = canvas.transform.FindChild("OptionsMenu(Clone)").transform.transform.FindChild("VideoPanel").gameObject;

        panelPos = videoPanel.transform.localPosition;

        videoPanel.transform.localPosition = optionsPanel.transform.localPosition;

        videoPanel.transform.FindChild("Quality").transform.GetChild(0).GetComponent<Dropdown>().interactable = true;
        videoPanel.transform.FindChild("Quality").transform.GetChild(0).GetComponent<Dropdown>().Select();
        videoPanel.transform.FindChild("Quality").transform.GetChild(0).GetComponent<Dropdown>().value = QualitySettings.GetQualityLevel();
        videoPanel.transform.FindChild("Quality").transform.GetChild(0).GetComponent<Dropdown>().onValueChanged.AddListener(delegate { ChangeQuality(videoPanel.transform.FindChild("Quality").transform.GetChild(0).GetComponent<Dropdown>()); });

        videoPanel.transform.FindChild("VSync").GetComponent<Toggle>().interactable = true;
        if (QualitySettings.vSyncCount == 0)
        {
            videoPanel.transform.FindChild("VSync").GetComponent<Toggle>().isOn = false;
        }
        else
        {
            videoPanel.transform.FindChild("VSync").GetComponent<Toggle>().isOn = true;
        }
        videoPanel.transform.FindChild("VSync").GetComponent<Toggle>().onValueChanged.AddListener(delegate { VSync(videoPanel.transform.FindChild("VSync").GetComponent<Toggle>()); });

        videoPanel.transform.FindChild("Back").GetComponent<Button>().interactable = true;
        videoPanel.transform.FindChild("Back").GetComponent<Button>().onClick.AddListener(delegate { VideoBackToOptions(videoPanel); });
    }

    public void ChangeQuality(Dropdown o)
    {
        switch (o.value)
        {
            case 0:
                QualitySettings.SetQualityLevel(o.value);
                break;
            case 1:
                QualitySettings.SetQualityLevel(o.value);
                break;
            case 2:
                QualitySettings.SetQualityLevel(o.value);
                break;
            case 3:
                QualitySettings.SetQualityLevel(o.value);
                break;
            case 4:
                QualitySettings.SetQualityLevel(o.value);
                break;
            case 5:
                QualitySettings.SetQualityLevel(o.value);
                break;
        }
    }

    public void VSync(Toggle t)
    {
        if(t.isOn)
        {
            QualitySettings.vSyncCount = 2;
        }
        else
        {
            QualitySettings.vSyncCount = 0;
        }
    }
    
    public void VideoBackToOptions(GameObject o)
    {
        o.transform.localPosition = panelPos;
        o.transform.GetChild(1).GetChild(0).GetComponent<Dropdown>().interactable = false;
        o.transform.GetChild(2).GetComponent<Toggle>().interactable = false;
        o.transform.GetChild(3).GetComponent<Button>().interactable = false;
        
        foreach( GameObject g in optionsButtons)
        {
            g.GetComponent<Button>().interactable = true;
        }
        optionsButtons[0].GetComponent<Button>().Select();
        videoPanelOpen = false;
    }
#endregion
    #region audio
    public void AudioPanel()
    {
        audioPanelOpen = true;
        foreach (GameObject g in optionsButtons)
        {
            g.GetComponent<Button>().interactable = false;
        }

        GameObject optionsPanel = canvas.transform.FindChild("OptionsMenu(Clone)").transform.FindChild("Panel").gameObject;
        audioPanel = canvas.transform.FindChild("OptionsMenu(Clone)").transform.FindChild("AudioPanel").gameObject;

        panelPos = audioPanel.transform.localPosition;

        audioPanel.transform.localPosition = optionsPanel.transform.localPosition;

        audioPanel.transform.FindChild("MasterVolume").GetChild(0).GetComponent<Slider>().interactable = true;
        audioPanel.transform.FindChild("MasterVolume").GetChild(0).GetComponent<Slider>().Select();
        audioPanel.transform.FindChild("MasterVolume").GetChild(0).GetComponent<Slider>().value = AudioListener.volume;
        audioPanel.transform.FindChild("MasterVolume").GetChild(0).GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeMasterVolume(audioPanel.transform.FindChild("MasterVolume").GetChild(0).GetComponent<Slider>()); });

        audioPanel.transform.FindChild("Mute").GetComponent<Toggle>().interactable = true;
        if(AudioListener.pause)
        {
            audioPanel.transform.FindChild("Mute").GetComponent<Toggle>().isOn = true;
        }
        else
        {
            audioPanel.transform.FindChild("Mute").GetComponent<Toggle>().isOn = false;
        }
        audioPanel.transform.FindChild("Mute").GetComponent<Toggle>().onValueChanged.AddListener(delegate { MuteAudio(audioPanel.transform.FindChild("Mute").GetComponent<Toggle>()); });

        audioPanel.transform.FindChild("Back").GetComponent<Button>().interactable = true;
        audioPanel.transform.FindChild("Back").GetComponent<Button>().onClick.AddListener(delegate { AudioBackToOptions(); });
    }

    public void ChangeMasterVolume(Slider s)
    {
        AudioListener.volume = s.value;
    }

    public void MuteAudio(Toggle t)
    {
        AudioListener.pause = t.isOn;
    }

    public void AudioBackToOptions()
    {
        audioPanel.transform.localPosition = panelPos;
        audioPanel.transform.GetChild(1).GetChild(0).GetComponent<Slider>().interactable = false;
        audioPanel.transform.GetChild(2).GetComponent<Toggle>().interactable = false;
        audioPanel.transform.GetChild(3).GetComponent<Button>().interactable = false;

        foreach (GameObject g in optionsButtons)
        {
            g.GetComponent<Button>().interactable = true;
        }
        optionsButtons[1].GetComponent<Button>().Select();
        audioPanelOpen = false;
    }

    #endregion
    #region controls

    public void ControlsPanel()
    {
        controlsPanelOpen = true;
        foreach (GameObject g in optionsButtons)
        {
            g.GetComponent<Button>().interactable = false;
        }

        GameObject optionsPanel = canvas.transform.FindChild("OptionsMenu(Clone)").transform.GetChild(1).gameObject;
        controlsPanel = canvas.transform.FindChild("OptionsMenu(Clone)").transform.FindChild("ControlsPanel").gameObject;

        panelPos = controlsPanel.transform.localPosition;

        controlsPanel.transform.localPosition = optionsPanel.transform.localPosition;

        controlsPanel.transform.FindChild("Back").GetComponent<Button>().interactable = true;
        controlsPanel.transform.FindChild("Back").GetComponent<Button>().onClick.AddListener(delegate { ControlsBackToOptions(); });
        controlsPanel.transform.FindChild("Back").GetComponent<Button>().Select();
    }

    void ControlsBackToOptions()
    {
        controlsPanel.transform.localPosition = panelPos;
        controlsPanel.transform.GetChild(2).GetComponent<Button>().interactable = false;

        foreach (GameObject g in optionsButtons)
        {
            g.GetComponent<Button>().interactable = true;
        }
        optionsButtons[2].GetComponent<Button>().Select();
        controlsPanelOpen = false;
    }

    void InvertY()
    {
        if (!camera.GetComponent<Camera_Control>().invert_cameraY)
        {
            camera.GetComponent<Camera_Control>().InvertY(true);
        }
        else
        {
            camera.GetComponent<Camera_Control>().InvertY(false);
        }
    }

    void InvertX()
    {
        if (!camera.GetComponent<Camera_Control>().invert_cameraX)
        {
            camera.GetComponent<Camera_Control>().InvertX(true);
        }
        else
        {
            camera.GetComponent<Camera_Control>().InvertX(false);
        }
    }
    #endregion

    
}
